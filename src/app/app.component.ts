import { Component } from '@angular/core';
import { Plato } from './interfaces/modelsFood.interface';
import datos from 'src/assets/json/datos.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  tipos1 = datos.tipos1;
  tipos2 = []
  tipos3 = []

  platos: Plato[] = []

  plato: Plato = {
    tipo1: "",
    tipo2: "",
    tipo3: ""
  }

  constructor() {
  }

  guardar() {
    console.log("guardando..");
    let plato: Plato = {
      tipo1:this.plato.tipo1,
      tipo2:this.plato.tipo2,
      tipo3:this.plato.tipo3
    }
    this.platos.push(plato)
  }

  onSelectTipo1(component?: any) {
    this.tipos2 = datos.tipos2[component.value]
    this.tipos3 = []
    this.plato.tipo2 = ''
     this.plato.tipo3 = ''
  }
  
  onSelectTipo2(component?: any) {
    this.tipos3 = datos.tipos3[component.value]
    this.plato.tipo3 = ""
  }

  limpiar():void {
    this.platos = []
    console.log("limpiando..");
  }
}
